package jtp2.threats;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;
import java.util.concurrent.RecursiveTask;

import org.apache.log4j.Logger;
import org.perf4j.StopWatch;

@SuppressWarnings("serial")
public class ForkJoinVis extends RecursiveTask<Double>{
	static Logger logger = Logger.getLogger(PathsListGetter.class.getName());
		double tab[];
		int tabLenght;
		int tabStart;
		
		public ForkJoinVis(double tab[],int tabStart, int tabLenght) {
			this.tab = tab;
			this.tabLenght = tabLenght;
			this.tabStart = tabStart;
			
		}
	@Override
	protected Double compute() {
		logger.info(" Thread Started");
		StopWatch watch = new StopWatch();
		watch.start();		
		Future<Double> a, b, c;
		ExecutorService pool = Executors.newFixedThreadPool(3);

		a = pool.submit(new Multiply(v.getX(), u.getX()));
		b = pool.submit(new Multiply(v.getY(), u.getY()));
		c = pool.submit(new Multiply(v.getZ(), u.getZ()));
		double q, w, e;
		q = w = e = 0;
		try {
			q = a.get();
			w = b.get();
			e = c.get();
		} catch (InterruptedException | ExecutionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		logger.info(watch.stop("methodBeingTimed.success"));
		logger.info(" Thread FINISHED");
	
		System.out.println("The result is: "+(e+w+q));
		logger.info("Time of thread " + watch.getElapsedTime());
		pool.shutdown();
		return null;

		return null;
	}
	public Double computeDirectly(){
		int i,j;
		i = this.tabStart;
		j = this.tabLenght;
		return this.tab[i]*this.tab[j];
		
	
	}
	public void start(){
		 Product fb = new Product(this.v,this.u);
		 ForkJoinPool pool = new ForkJoinPool();
		 pool.invoke(fb);
	}
}
