package jtp2.threats;

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

public class OpenImg extends Applet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	 private BufferedImage img;
	 
     public void init() {
         try {
             URL url = new URL(getCodeBase(), "/home/tom/kot.jpg");
             img = ImageIO.read(url);
         } catch (IOException e) {
         }
     }
 
     public void paint(Graphics g) {
       g.drawImage(img, 1000, 0, null);
     }

}
