package jtp2.threats;

import java.util.concurrent.RecursiveAction;

public class RecusiveMul extends RecursiveAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5302576244176699480L;
	private Double[] vectorU;
	private Double[] vectorV;
	private int vStart;
	private int vLength;
	private Double result = (double) 0;



	

	

	public RecusiveMul(Double[] u, Double[] v, int vStart, int vLength,
			Double res) {
		
			this.vectorU = u;
			this.vectorV = v;
			this.vStart = vStart;
			this.vLength = vLength;
			this.result = res;
		}

	

	protected void computeDirectly() {
		double counter = 0;

		for (int i = vStart; i < vLength; ++i) {
			counter += vectorU[i] * vectorV[i];
		}
		//System.out.println(counter);
		result += counter; 
		return;
	}

	protected void compute() {
	
		if (vLength - vStart< 3 ) {
			computeDirectly();
			
		} else {
			int split = (vLength + vStart )/2;
			invokeAll(new RecusiveMul(vectorU, vectorV, vStart, split, result), new RecusiveMul(vectorU, vectorV, split, vLength, result));
	
		}
		
		return;
	}

}
