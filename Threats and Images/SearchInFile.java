package jtp2.threats;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class SearchInFile extends Thread {
	private String pathToFile;
	private String[] text;

	public SearchInFile(String pathToFile, String[] text) {
		this.pathToFile = pathToFile;
		this.text = text;

	}

	public void run() {

		try (Scanner sc = new Scanner(new File(pathToFile))) {
			int i = 0;
			while (sc.hasNext()) {
				String comp = sc.next().toLowerCase().trim();
				if (comp.equals(text[i])) {
					++i;
				} else {
					i = 0;
				}
				if (i == text.length) {
					System.out.println("I found in   " + pathToFile);
					return;
				}
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
