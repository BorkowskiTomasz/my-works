package jtp2.threats;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.log4j.Logger;

public class PathsListGetter extends Thread {
	static Logger logger = Logger.getLogger(PathsListGetter.class.getName());
	private Path path;

	public PrintFiles pf = new PrintFiles();

	public PathsListGetter(Path path, String pattern) {
		this.path = path;
		pf.setPattern(pattern);
	}

	public void setPath(Path path) {
		this.path = path;
	}

	@Override
	public void run() {
		logger.info("PrintFile Thread Started");
		try {
			Files.walkFileTree(path, pf);
		} catch (IOException e) {
			logger.error("IOExeption",e);
		}

	}

}
