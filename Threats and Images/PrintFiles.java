package jtp2.threats;

import static java.nio.file.FileVisitResult.CONTINUE;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.LinkedList;

import org.apache.log4j.Logger;

public class PrintFiles extends SimpleFileVisitor<Path> {
	static Logger logger = Logger.getLogger(PathsListGetter.class.getName());
	LinkedList<String> paths = new LinkedList<String>();
	String pattern;

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public LinkedList<String> getList() {
		return paths;

	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {

		if (attr.isRegularFile()
				& file.getFileName().toString().endsWith(pattern)) {
			paths.add(file.getParent().toString() + "/"
					+ file.getFileName().toString());

		}

		return CONTINUE;
	}

	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) {

		return CONTINUE;
	}

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) {
		System.err.println(exc);
		return CONTINUE;
	}

}
