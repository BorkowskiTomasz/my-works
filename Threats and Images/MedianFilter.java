package jtp2.threats;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class MedianFilter implements ImageFilter {

	private BufferedImage img;
	private File input;
	private String output;

	public static final int[] dx = { -1, 0, 1, -1,  1, -1, 0, 1};
	public static final int[] dy = { -1, -1, -1, 0,  0, 1, 1, 1 };

	public  MedianFilter(File input, String output){
		this.input = input;
		this.output = output;
	}

	public void process() {
		try {
			img = ImageIO.read(input);
		int width = img.getWidth();
		int height = img.getHeight();
		BufferedImage filtered = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_ARGB);
		filtered = img;
		
		for (int y = 1; y < height - 1; y ++)
			for (int x = 1; x < width - 1; x ++) {

				int counter = img.getRGB(x, y);
				for (int i = 0; i < 8 ; i++) {
					counter += img.getRGB(x + dx[i], y + dy[i]);
				}
				counter = counter / 8;
				filtered.setRGB(x, y, counter);
			}

		String format = "jpg";
		File saveFile = new File(output);
	
			ImageIO.write(filtered, format, saveFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
