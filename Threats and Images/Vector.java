package jtp2.threats;

public class Vector {
	private double x;
	private double y;
	private double z = 0;
	private int dim;

	int getDim() {
		return this.dim;
	}

	public Vector(double x, double y) {
		this.x = x;
		this.y = y;
		this.dim = 2;
	}

	public Vector(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.dim = 3;
	}

	public double getX() {
		return x;
	}

	public double getZ() {
		return z;
	}

	public double getY() {
		return y;
	}
}
