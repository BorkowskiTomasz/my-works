package jtp2.threats;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;

import org.apache.log4j.Logger;

public class ThreatedSearcher {
	static Logger logger = Logger.getLogger(ThreatedSearcher.class.getName());

	public static void main(String[] args) {
		Path path = Paths.get("/home/tom");
		String pattern = ".txt";
		String text[] = { "kon", "jest", "misiem" };
		
		PathsListGetter sciezka = new PathsListGetter(path, pattern);
		(sciezka).start();
		logger.info("Thread" + sciezka.getName() + " runned");
		LinkedList<String> list;
		SearchInFile search = null;
		while (!(list = sciezka.pf.paths).isEmpty() || sciezka.isAlive()) {
			if (list.isEmpty()) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					logger.error("InterruptedExeption",e);
				}
			} else {
				String pathToFile = list.poll();

				(search = new SearchInFile(pathToFile, text)).start();

			}
		}
		if (search.isAlive()) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.error("InterruptedExeption",e);
			}

		}
		logger.info("All is done. Threats is dead");
		return;
	}
}
