package jtp2.threats;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;

public class MeanFilter implements ImageFilter {
	private BufferedImage img;
	private File input;
	private String output;
	public static final int[] dx = { -1, 0, 1, -1, 1, -1, 0, 1,0 };
	public static final int[] dy = { -1, -1, -1, 0, 0, 1, 1, 1,0 };

	public MeanFilter(File input, String output) {
		this.input = input;
		this.output = output;
	}

	public void process() {
		try {
			img = ImageIO.read(input);
			int width = img.getWidth();
			int height = img.getHeight();
			BufferedImage filtered = new BufferedImage(width, height,
					BufferedImage.TYPE_INT_ARGB);
			filtered = img;
			int[] tab = new int[9];
			for (int y = 1; y < height - 1; y++)
				for (int x = 1; x < width - 1; x++) {

					int counter = img.getRGB(x, y);
					for (int i = 0; i < 9; i++) {
						tab[i] = img.getRGB(x + dx[i], y + dy[i]);
					}
					Arrays.sort(tab);
					counter = tab[4];
					filtered.setRGB(x, y, counter);
				}

			String format = "jpg";
			File saveFile = new File(output);

			ImageIO.write(filtered, format, saveFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
