package jtp2.threats;

import java.util.LinkedList;
import java.util.Set;

import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
	
import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

public class Reflelction {
	public static void main(String[] args){
//		 Reflections reflections = new Reflections("jtp2.threats");
//
//		 Set<Class<? extends Object>> allClasses = 
//		     reflections.getSubTypesOf(Object.class);
//		for( Object i : allClasses ){
//			System.out.println("Name: " + 	i.toString());
//		}
		
		List<ClassLoader> classLoadersList = new LinkedList<ClassLoader>();
		classLoadersList.add(ClasspathHelper.contextClassLoader());
		classLoadersList.add(ClasspathHelper.staticClassLoader());

		Reflections reflections = new Reflections(new ConfigurationBuilder()
		    .setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner())
		    .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
		    .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("com.jtp2.home/src"))));
		
		Set<Class<?>> classes = reflections.getSubTypesOf(Object.class);
	}
}
