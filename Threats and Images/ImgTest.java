package jtp2.threats;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JComboBox;
import javax.swing.text.html.HTMLDocument.HTMLReader.HiddenAction;

import org.apache.logging.log4j.core.filter.BurstFilter;

public class ImgTest {
	 private static int colorToRGB(int alpha, int red, int green, int blue) {
         int newPixel = 0;
         newPixel += alpha;
         newPixel = newPixel << 8;
         newPixel += red; newPixel = newPixel << 8;
         newPixel += green; newPixel = newPixel << 8;
         newPixel += blue;

         return newPixel;
     }

	public static void main(String[] args) {
		BufferedImage img;
		String format = "jpg";
		File saveFile = new File("savedimage." + format);
		try {
			img = ImageIO.read(new File("/home/tom/test.png"));
			int width = img.getWidth();
			int height = img.getHeight();
			Graphics big = img.getGraphics();
			big.drawImage(img, 1000, 0, null);
			BufferedImage helf = new BufferedImage(width, height,
					BufferedImage.TYPE_INT_ARGB);
			helf = img;
		    int[] avgLUT = new int[766];
		    for(int i=0; i<avgLUT.length; i++) avgLUT[i] = (int) (i / 3);
			for (int j = 0; j < height; j++)
				for (int i = 0; i < width; i++) {
					int alpha = new Color(helf.getRGB(i, j)).getAlpha();
					int blue = new Color(helf.getRGB(i, j)).getBlue();
					int red = new Color(helf.getRGB(i, j)).getRed();
					int green = new Color(helf.getRGB(i, j)).getGreen();
					red = (int) (0.21 * red + 0.71 * green + 0.07 * blue);
			            // Return back to original format
			            int newPixel = colorToRGB(alpha, red, red, red);
			
					helf.setRGB(i, j, newPixel );
					
					
					
				}
			
			ImageIO.write(helf, format, saveFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			img = ImageIO.read(new File("/home/tom/kot.jpg"));
			MedianFilter filter = new MedianFilter(img);
			filter.process();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
