package jtp2.threats;

import java.util.concurrent.Callable;

public class Multiply implements Callable<Double> {
	double var1;
	double var2;
		public Multiply(double var1, double var2){
		this.var1 = var1;
		this.var2 = var2;
		}
	public Double call() throws Exception {
		
		
		return var1*var2;
	}

}
