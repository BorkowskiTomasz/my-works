package jtp2.threats;

import java.io.File;
import java.lang.reflect.Field;

import org.apache.log4j.Logger;

public class PrivateFieldInject {
	static Logger logger = Logger.getLogger(PrivateFieldInject.class.getName());
	public static void main(String[] args){
		 File file = new File("/home/tom/kot.jpg");
		 FinalImageFilter img = new FinalImageFilter(file, "Private.jpg");
		 try {
		 Field privateStringField = FinalImageFilter.class
		 .getDeclaredField("filter");
		
		 privateStringField.setAccessible(true);
		
		 String fieldValue = (String) privateStringField.get(img).toString();
		 System.out.println("fieldValue = " + fieldValue);
		 BlackAndWhite value = new BlackAndWhite(file, "Private.jpg");
		 privateStringField.set(img, value);
		 
		 
		ImageFilter ada = img.getFilter();
		 ada.process();
		 
		 } catch (NoSuchFieldException | SecurityException |
		 IllegalArgumentException | IllegalAccessException e) {
			 logger.error("InterruptedExeption",e);
		 }
		

		
	}
}
