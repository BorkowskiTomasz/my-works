package jtp2.threats;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;

public class ScalarMultiply {

	public static void main(String[] args) {
		int vLength = 20000000;
			Double[] v = new Double[vLength];
			Double[] u = new Double[vLength];
			double res = 0;
			int vStart = 0;
		Random rand = new Random();
		for(int i  = 0 ; i < vLength ; i++){
			v[i]=rand.nextDouble()*vLength;
			u[i]=rand.nextDouble()*vLength;
			//System.out.println("v: "+v[i]+"\t u: "+u[i] );
		}
		RecusiveMul mul = new RecusiveMul(u, v, vStart, vLength, res) ;
        ForkJoinPool pool = new ForkJoinPool();

        long startTime = System.currentTimeMillis();
        pool.invoke(mul);
        long endTime = System.currentTimeMillis();

        System.out.println(" took " + (endTime - startTime) + 
                " milliseconds.");
    
        double ina = 0;
        startTime = System.currentTimeMillis();
        for(int i  = 0 ; i < vLength ; i++){
			ina += v[i]*u[i];
			//System.out.println("v: "+v[i]+"\t u: "+u[i] );
		}
        endTime = System.currentTimeMillis();
      //  System.out.println("Secccc " + (endTime - startTime) + 
        //        " milliseconds.");
                System.out.println(ina);
	}
}
