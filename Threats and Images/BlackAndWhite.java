package jtp2.threats;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class BlackAndWhite implements ImageFilter {
	private BufferedImage img;
	private BufferedImage helf;
	private File input;
	private String output;
	public BlackAndWhite(File input, String output) {
		this.input = input;
		this.output = output;
		
	}
	 private static int colorToRGB(int alpha, int red, int green, int blue) {
         int newPixel = 0;
         newPixel += alpha;
         newPixel = newPixel << 8;
         newPixel += red; newPixel = newPixel << 8;
         newPixel += green; newPixel = newPixel << 8;
         newPixel += blue;

         return newPixel;
     }
	@Override
	public void process() {
		
		try {
			img = ImageIO.read(input);
			int width = img.getWidth();
			int height = img.getHeight();
			Graphics big = img.getGraphics();
			big.drawImage(img, 1000, 0, null);
			helf = new BufferedImage(width, height,
					BufferedImage.TYPE_INT_ARGB);
			helf = img;
		
			for (int j = 0; j < height; j++)
				for (int i = 0; i < width; i++) {
					int alpha = new Color(helf.getRGB(i, j)).getAlpha();
					int blue = new Color(helf.getRGB(i, j)).getBlue();
					int red = new Color(helf.getRGB(i, j)).getRed();
					int green = new Color(helf.getRGB(i, j)).getGreen();
					red = (int) (0.21 * red + 0.71 * green + 0.07 * blue);
			         
			            int newPixel = colorToRGB(alpha, red, red, red);
			
					helf.setRGB(i, j, newPixel );
					
					
					
				}
			String format = "jpg";
			File saveFile = new File(output);
			ImageIO.write(helf, format, saveFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
