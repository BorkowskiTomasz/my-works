package jtp2.threats;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ConstTranslatorHelper {
	static Logger logger = Logger.getLogger(PrivateFieldInject.class.getName());

	public static void main(String[] args) {

		String name = "al";
		Field[] objClass = Const.class.getFields();
		
		Properties prop = new Properties();	
		OutputStream output = null;
		
		try {	
			output = new FileOutputStream(name+".properties");
		
			for (Field var : objClass) {
				prop.setProperty(var.getName(), (String) var.get(var));
			}
		
			prop.store(output, null);
		} catch (IOException | IllegalArgumentException
				| IllegalAccessException e) {
			logger.error("InterruptedExeption",e);
		} finally {

		}

	}

}
