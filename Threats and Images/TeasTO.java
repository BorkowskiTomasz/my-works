package jtp2.threats;

import org.apache.log4j.Logger;
import org.perf4j.StopWatch;

public class TeasTO {
	static Logger logger = Logger.getLogger(PathsListGetter.class.getName());

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		logger.info(" TeasTO Started");
		StopWatch watch1 = new StopWatch();
		watch1.start();
		Vector u = new Vector(1315512488, 1355232515, 1512536513);
		Vector v = new Vector(1355232515, 1325452310, 1355232515);
		Product prod = new Product(u, v);
		prod.start();
		StopWatch watch = new StopWatch();
		watch.start();
		logger.info(" Non-Thread Procedure Started");
		Double equal = (double) 0;
		equal = u.getX() * v.getX() + u.getY() * v.getY() + u.getZ() * v.getZ();

		watch1.stop();

		logger.info(watch1.stop("methodBeingTimed.success"));
		logger.info(" Procedure FINISHED");
		System.out.println("The result is: "+equal);
		logger.info("Time of non Thread procedure  " + watch1.getElapsedTime());
	}
}
