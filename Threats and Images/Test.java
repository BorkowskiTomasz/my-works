package jtp2.threats;

import java.io.File;

public class Test {

	public static void main(String[] args) {
		File file = new File("/home/tom/MeanFiltered.jpg");
		MeanFilter img = new MeanFilter(file, "MeanFiltered.jpg");
		img.process();
		MedianFilter imga = new MedianFilter(file, "MedianFiltered.jpg");
		imga.process();
		BlackAndWhite im = new BlackAndWhite(file, "B&W");
		im.process();
	}

}
