import java.util.ArrayList;
import java.util.Random;

public class VectorNorm {

	public static void main(String[] args) {
		ArrayList<Double> vector = new ArrayList<Double>();
		Random random = new Random();
		for (int i = 0; i < 1000; i++) {
			vector.add(random.nextDouble());

		}
//		vector.add((double) 4);
//		vector.add((double) 3);
		Double norm = Math.sqrt(vector.stream().map( x-> {return (x*x);}).reduce(Double::sum).get());
		//Double norm = Math.sqrt(vector.stream().map( x-> {return (x*x);}).reduce());
		System.out.print(norm);
	}
}
