
import java.util.Arrays;
import java.util.Random;

public class Sorts {

	public static void main(String[] args) {
		Random random = new Random();
		Double[] tab = new Double[100000];
		Double[] tab1 = new Double[100000];
		for(int i = 0 ; i < 100000 ; i++){
			tab[i] = random.nextDouble();
			tab1[i] = random.nextDouble();
		}
		
		long start = System.currentTimeMillis();
		Arrays.sort(tab);
		long end = System.currentTimeMillis();
		System.out.println(end - start);
		start = System.currentTimeMillis();
		Arrays.parallelSort(tab1);
		end = System.currentTimeMillis();
		System.out.println(end - start);
	}

}
