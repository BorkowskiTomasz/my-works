package com.project.greenote.client.mainview;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class MainView {

	private String login;

	private NoteServiceAsync noteSvc;

	private AbsolutePanel mainPanel = new AbsolutePanel();

	private FlexTable flexTable = new FlexTable();
	private FocusPanel focusPanel = new FocusPanel();
	private VerticalPanel addPanel = new VerticalPanel();
	private TextBox titleBox = new TextBox();
	private TextArea textArea = new TextArea();
	private Button addButton = new Button("ADD");
	private Label loginInfoLabel = new Label();
	private HashMap<String, Integer> titles = new HashMap<>();
	private HorizontalPanel topLabel = new HorizontalPanel();
	private Button logOutButton = new Button();
	private Button deleteAccountButton = new Button("");
	private final HorizontalPanel horizontalPanel = new HorizontalPanel();
	private final Label titleLabel = new Label("Title:");

	public MainView(String login) {

		this.login = login;
		this.noteSvc = GWT.create(NoteService.class);
		buildFlexTable();
		topLabel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		mainPanel.add(topLabel);
		topLabel.setSize("800px", "24px");
		topLabel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);

		// welcoming user

		Label userInfoLabe = new Label("Hello, " + login);
		userInfoLabe.setStyleName("welcome-panel");
		topLabel.add(userInfoLabe);
		userInfoLabe.setSize("592px", "23px");
		topLabel.setCellHorizontalAlignment(userInfoLabe,
				HasHorizontalAlignment.ALIGN_RIGHT);

		mainPanel.addStyleName("Main");

		// setting logout button
		logOutButton.setTitle("Log out");
		logOutButton.setStyleName("logout-button");

		topLabel.add(logOutButton);

		// setting remove account button
		deleteAccountButton.setTitle("Remove your Account");
		deleteAccountButton.setStyleName("remove-button");
		topLabel.add(deleteAccountButton);

		mainPanel.add(flexTable);
		flexTable.setSize("800px", "30px");

		loadNotes(login);
		final String log = login;
		// smart method
		Button button = new Button();
		button.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				loadNotes(log);
			}
		});
		button.addKeyPressHandler(new KeyPressHandler() {

			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_F5) {
					loadNotes(log);
				}

			}
		});

		mainPanel.add(focusPanel);
		focusPanel.setSize("230px", "240px");

		addPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		focusPanel.setWidget(addPanel);

		addPanel.setSize("100%", "100%");

		addPanel.add(horizontalPanel);
		horizontalPanel.setSize("230", "30");

		horizontalPanel.add(titleLabel);
		horizontalPanel.setCellVerticalAlignment(titleLabel,
				HasVerticalAlignment.ALIGN_MIDDLE);
		horizontalPanel.setCellHorizontalAlignment(titleLabel,
				HasHorizontalAlignment.ALIGN_CENTER);
		titleLabel.setSize("48px", "25px");
		titleLabel.setStyleName("add-title-cell");
		horizontalPanel.add(titleBox);

		titleBox.setName("Title");
		titleBox.setTitle("Uniqe Title");
		titleBox.setWidth("181px");

		addPanel.add(textArea);

		textArea.setSize("100%", "147px");
		// setting addButton
		addButton.setStyleName("add-button");

		addPanel.add(addButton);
		mainPanel.add(loginInfoLabel);

		addPanel.setCellHorizontalAlignment(addButton,
				HasHorizontalAlignment.ALIGN_CENTER);
		addPanel.setCellVerticalAlignment(addButton,
				HasVerticalAlignment.ALIGN_MIDDLE);

		// Setting FlexTable

		// flexTable.addStyleName("FlexTable");

		addButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				String title = titleBox.getText();
				String text = textArea.getText();
				addNote(title, text);

			}

		});

	}

	// add note button
	private boolean addNote(String title, String text) {

		loginInfoLabel.setVisible(false);
		if (noteSvc == null) {
			noteSvc = GWT.create(NoteService.class);
		}
		if (title.length() > 40) {
			loginInfoLabel.setText("Error: Title is too long ");
			loginInfoLabel.setVisible(true);
			return false;
		}

		AsyncCallback<String[]> callback = new AsyncCallback<String[]>() {

			@Override
			public void onFailure(Throwable caught) {
				String details = caught.toString();

				loginInfoLabel.setText("Error: " + details);
				loginInfoLabel.setVisible(true);

				return;
			}

			@Override
			public void onSuccess(final String[] result) {
				if (result == null)
					return;
				textArea.setText("");
				titleBox.setText("");
				final int row = flexTable.getRowCount();
				flexTable.setText(row, 0, result[3]);
				flexTable.setText(row, 1, result[2]);
				flexTable.setText(row, 2, result[4]);

				titles.put(result[3], row);
				// setting visual of flexTable
				flexTable.getCellFormatter().addStyleName(row, 0,
						"FlexTable-Title-Column");
				flexTable.getCellFormatter().addStyleName(row, 1,
						"FlexTable-Cell");
				flexTable.getCellFormatter().addStyleName(row, 2,
						"FlexTable-Date-Column");
				flexTable.getCellFormatter().addStyleName(row, 3,
						"FlexTable-Date-Column");

				Button deleteButton = new Button("X");

				flexTable.setWidget(row, 3, deleteButton);
				deleteButton.setStyleName("delete-button");

				deleteButton.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						flexTable.removeRow(row);
						deleteNote(result[3]);
					}
				});

			}
		};

		Date date = new Date();
		DateTimeFormat dtf = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm:ss");
		Integer id = flexTable.getRowCount();

		noteSvc.addNote(id + 1, login, title, text,
				dtf.format(date, TimeZone.createTimeZone(-120)), callback);

		return false;

	}

	// loading and adding all notes from database
	private boolean loadNotes(String login) {

		buildFlexTable();
		flexTable.setCellSpacing(0);

		loginInfoLabel.setVisible(false);

		if (noteSvc == null) {
			noteSvc = GWT.create(NoteService.class);
		}

		AsyncCallback<ArrayList<String[]>> callback = new AsyncCallback<ArrayList<String[]>>() {

			@Override
			public void onFailure(Throwable caught) {
				String details = caught.toString();

				loginInfoLabel.setText("Error: " + details);
				loginInfoLabel.setVisible(true);

				return;
			}

			@Override
			public void onSuccess(final ArrayList<String[]> result) {
				if (result != null)
					for (final String[] arg : result) {

						final int row = flexTable.getRowCount();
						titles.put(arg[3], row);
						flexTable.setText(row, 0, arg[3]);
						flexTable.setText(row, 1, arg[2]);
						flexTable.setText(row, 2, arg[4]);

						// flexTable.getCellFormatter().addStyleName(row, 1,
						// "FlexTable-Cell");

						// seting table

						flexTable.getCellFormatter().addStyleName(row, 0,
								"FlexTable-Title-Column");
						flexTable.getCellFormatter().addStyleName(row, 1,
								"FlexTable-Cell");
						flexTable.getCellFormatter().addStyleName(row, 2,
								"FlexTable-Date-Column");
						flexTable.getCellFormatter().addStyleName(row, 3,
								"FlexTable-Date-Column");

						Button deleteButton = new Button("X");
						deleteButton.setStyleName("delete-button");
						flexTable.setWidget(row, 3, deleteButton);

						deleteButton.addClickHandler(new ClickHandler() {

							@Override
							public void onClick(ClickEvent event) {
								flexTable.removeRow(row);
								deleteNote(arg[3]);
							}
						});

					}
			//	titleBox.getElement().focus();

			}

		};
		noteSvc.getNotes(login, callback);

		return false;

	}

	// deleting notes and load notes
	private boolean deleteNote(String title) {
		loginInfoLabel.setVisible(false);
		if (noteSvc == null) {
			noteSvc = GWT.create(NoteService.class);
		}
		AsyncCallback<Boolean> callback = new AsyncCallback<Boolean>() {

			@Override
			public void onFailure(Throwable caught) {
				String details = caught.toString();

				loginInfoLabel.setText("Error: " + details);
				loginInfoLabel.setVisible(true);

			}

			@Override
			public void onSuccess(Boolean result) {
				loginInfoLabel.setText("Deleted");
				loginInfoLabel.setVisible(true);
				loadNotes(login);

			}

		};
		noteSvc.deleteNote(0, "", title, "", "", callback);
		return true;
	}

	public boolean deleteUserAccount() {
		loginInfoLabel.setVisible(false);
		if (noteSvc == null) {
			noteSvc = GWT.create(NoteService.class);
		}
		AsyncCallback<Boolean> callback = new AsyncCallback<Boolean>() {

			@Override
			public void onFailure(Throwable caught) {

				Window.Location.reload();
			}

			@Override
			public void onSuccess(Boolean result) {

			}
		};

		noteSvc.deleteAccount(login, callback);

		return false;

	}

	private void buildFlexTable() {

		flexTable.removeAllRows();
		flexTable.setText(0, 0, " Title");
		flexTable.setText(0, 1, " Text");
		flexTable.setCellSpacing(12);

		flexTable.setText(0, 2, " Date");
		flexTable.setText(0, 3, " Delete");

		// adding style name to LabelCells

		flexTable.getCellFormatter().addStyleName(0, 0, "FlexTable-TitleCells");
		flexTable.getCellFormatter().addStyleName(0, 1, "FlexTable-TitleCells");
		flexTable.getCellFormatter().addStyleName(0, 2, "FlexTable-TitleCells");
		flexTable.getCellFormatter().addStyleName(0, 3, "FlexTable-TitleCells");

	}

	// getters and setters

	public Button getLogOutButton() {
		return logOutButton;
	}

	public void setLogOutButton(Button logOutButton) {
		this.logOutButton = logOutButton;
	}

	public Button getDeleteAccountButton() {
		return deleteAccountButton;
	}

	public void setDeleteAccountButton(Button deleteAccountButton) {
		this.deleteAccountButton = deleteAccountButton;
	}

	public TextBox getTitleBox() {
		return titleBox;
	}

	public void setTitleBox(TextBox titleBox) {
		this.titleBox = titleBox;
	}

	public AbsolutePanel getMainPanel() {
		return mainPanel;
	}

	public void setMainPanel(AbsolutePanel mainPanel) {
		this.mainPanel = mainPanel;
	}

}
