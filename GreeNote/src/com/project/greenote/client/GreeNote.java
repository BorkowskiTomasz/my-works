package com.project.greenote.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.project.greenote.client.loginview.LoginService;
import com.project.greenote.client.loginview.LoginServiceAsync;
import com.project.greenote.client.loginview.LoginView;
import com.project.greenote.client.mainview.MainView;

public class GreeNote implements EntryPoint {

	private LoginServiceAsync loginSvc = GWT.create(LoginService.class);

	private HorizontalPanel topLabel = new HorizontalPanel();
	private VerticalPanel mainPanel = new VerticalPanel();
	private LoginView loginView;
	private MainView mainView = null;
	private RootPanel rootPanel = null;
	private String login;

	public void onModuleLoad() {
		rootPanel = RootPanel.get("stockList");

		rootPanel.add(mainPanel);
		mainPanel.add(topLabel);
		topLabel.setSize("240px", "14px");
		loginView = new LoginView(loginSvc);

		mainPanel.add(loginView.getMainPanel());
		loginView.getTxtbxLogin().setFocus(true);

		loginView.getFinalButton().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (loginView.isResultLog()) {
					loginView.getDialogBox().setVisible(false);
					changeView();
					login = loginView.getLogin();
					prepareToDie();
				}
			}
		});

	}

	protected void prepareToDie() {
		mainView.getDeleteAccountButton().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				System.out.println("KLIK");
				mainView = new MainView(loginView.getLogin());
				mainView.deleteUserAccount();
				loginView.deleteUserAccount(login);
				logOut();

			}
		});
	}

	public void changeView() {
		mainView = new MainView(loginView.getLogin());

		mainPanel.clear();
		mainPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);

		mainPanel.add(mainView.getMainPanel());
		mainView.getLogOutButton().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				logOut();
			};

		});

	}

	protected void logOut() {
		login = null;
		mainPanel.clear();
		rootPanel.clear();
		loginView = null;
		mainView = null;
		Window.Location.reload();
		onModuleLoad();

	}
}
