package com.project.greenote.client;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface Repo extends ClientBundle {
	@Source("BGpaper.png")
	ImageResource myBG();
	
}
