package com.project.greenote.server.loginview;

import org.hibernate.HibernateError;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.googlecode.gwt.crypto.bouncycastle.DataLengthException;
import com.googlecode.gwt.crypto.bouncycastle.InvalidCipherTextException;
import com.googlecode.gwt.crypto.client.TripleDesCipher;
import com.project.greenote.client.loginview.LoginException;
import com.project.greenote.client.loginview.LoginService;
import com.project.greenote.server.HibernateUtil;

import de.novanic.eventservice.service.RemoteEventServiceServlet;

public class LoginServiceImpl extends RemoteEventServiceServlet implements
		LoginService {

	private static final long serialVersionUID = -8137563732356372118L;

	public boolean getLoginData(String login, String password)
			throws LoginException {
		try {
			// make a connection
			Transaction trns = null;
			Session session = null;
			session = HibernateUtil.getSessionFactory().openSession();
			if (session == null) {
				throw new LoginException("Error Creating Session");
			}
			if (login.equals("") || password.equals("")) {
				throw new LoginException("Empty login or password");
			}
			try {
				GreeUser user = new GreeUser(login, password);
				trns = session.beginTransaction();
				GreeUser usr = null;
				usr = (GreeUser) session.get(GreeUser.class, login);
				if (usr == null) {
					throw new LoginException(
							"This login doesn't exist in database");
				}
				session.getTransaction().commit();

				// decrypting acctual password
				String decrypted = user.getPassword();
				// validing password
				if (decrypted.equals(password)) {
					return true;
				} else {
					return false;
				}
			} catch (RuntimeException e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.flush();
				session.close();
			}

		} catch (ExceptionInInitializerError e) {
			e.printStackTrace();

		}
		return false;

	}

	@Override
	public String createNewUser(String[] usr) throws LoginException {
		try {
			Transaction trns = null;
			Session session = null;
			session = HibernateUtil.getSessionFactory().openSession();
			if (session == null) {
				throw new LoginException("Error Creating Session");
			}
			try {

				// crypting password
				String password = usr[1];
				String encrypted = encrypt(password);

				GreeUser user = new GreeUser(usr[0], encrypted);

				trns = session.beginTransaction();
				String login = usr[0];
				password = usr[1];
				if (login.equals(""))
					throw new LoginException("Your login shoudn't be empty");
				if (password.equals(""))
					throw new LoginException("Your password shoudn't be empty");

				GreeUser ur = null;
				ur = (GreeUser) session.get(GreeUser.class, login);

				if (ur != null) {
					throw new LoginException("Login USED, Try another one.");
				}

				try {

					session.save(user);
				} catch (HibernateError e) {
					throw new LoginException(e.toString());
				}
				session.getTransaction().commit();
				// start();
				return "ok";
			} catch (RuntimeException e) {
				if (trns != null) {
					trns.rollback();
				}
				// logger.error("Coudnl't transmit", e);
				e.printStackTrace();
			} finally {
				session.flush();
				session.close();
			}

		} catch (ExceptionInInitializerError e) {
			e.printStackTrace();
			// logger.error("Coudnl't Init", e);
		}
		return "Something goes wrong.";

	}

	@Override
	public boolean deleteUserAccount(String login) throws LoginException {
		try {
			Transaction trns = null;
			Session session = null;
			session = HibernateUtil.getSessionFactory().openSession();
			if (session == null) {
				throw new LoginException("Error Creating Session");
			}

			if (login == null)
				throw new LoginException("Login doesn't exist");

			try {

				trns = session.beginTransaction();

				try {
					GreeUser test = null;
					test = (GreeUser) session.get(GreeUser.class, login);
					if (test == null)
						throw new LoginException("User doesn't exist");

					session.delete(test);

				} catch (HibernateError e) {
					throw new LoginException(e.toString());
				}
				session.getTransaction().commit();
				return true;
			} catch (RuntimeException e) {
				if (trns != null) {
					trns.rollback();
				}
				// logger.error("Coudnl't transmit", e);
				e.printStackTrace();
			} finally {
				session.flush();
				session.close();
			}

		} catch (ExceptionInInitializerError e) {
			e.printStackTrace();
			// logger.error("Coudnl't Init", e);

		}
		return false;
	}

	private byte[] GWT_DES_KEY = "9070AA94CEBD525C".getBytes();

	private String encrypt(String password) {
		String encrypted = null;
		TripleDesCipher cipher = new TripleDesCipher();
		cipher.setKey(GWT_DES_KEY);
		try {
			encrypted = cipher.encrypt(String.valueOf(password));
		} catch (DataLengthException e1) {
			e1.printStackTrace();
		} catch (IllegalStateException e1) {
			e1.printStackTrace();
		} catch (InvalidCipherTextException e1) {
			e1.printStackTrace();
		}
		return encrypted;

	}

	@SuppressWarnings("unused")
	private String derypt(String password) {
		String decrypted = null;
		TripleDesCipher cipher = new TripleDesCipher();
		cipher.setKey(GWT_DES_KEY);
		try {
			decrypted = cipher.decrypt(password);
		} catch (DataLengthException e) {
			e.printStackTrace();
			// logger.error("Data Lenght problem", e);
		} catch (IllegalStateException e) {
			e.printStackTrace();
			// logger.error("Baad Data", e);
		} catch (InvalidCipherTextException e) {
			e.printStackTrace();
			// logger.error("Coudnl't Init", e);
		}
		cipher = null;

		return decrypted;

	}
}
