package com.project.greenote.server.mainview;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateError;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.project.greenote.client.mainview.NoteException;
import com.project.greenote.client.mainview.NoteService;
import com.project.greenote.server.HibernateUtil;

public class NoteServiceImpl extends RemoteServiceServlet implements
		NoteService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2997161515689462600L;

	@Override
	public String[] addNote(Integer id, String login, String title,
			String text, String date) throws NoteException {

		try {
			Transaction trns = null;
			Session session = null;
			session = HibernateUtil.getSessionFactory().openSession();
			if (session == null) {
				throw new NoteException("Error Creating Session");
			}
			try {

				trns = session.beginTransaction();

				Notes note = new Notes(id, login, title, text, date);

				if (login == null) {
					throw new NoteException("Error with login!");
				}

				try {
					Notes test = null;
					test = (Notes) session.get(Notes.class, title);
					if (test != null)
						throw new NoteException(
								"Note with this Title already exist");
					session.save(note);
				} catch (HibernateError e) {
					throw new NoteException(e.toString());
				}
				session.getTransaction().commit();
				String[] result = null;
				result = note.toStrings();

				return result;
			} catch (RuntimeException e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.flush();
				session.close();
			}

		} catch (ExceptionInInitializerError e) {
			e.printStackTrace();

		}

		return null;
	}

	@Override
	public boolean deleteNote(Integer id, String login, String title,
			String text, String date) throws NoteException {
		try {
			Transaction trns = null;
			Session session = null;
			session = HibernateUtil.getSessionFactory().openSession();
			if (session == null) {
				throw new NoteException("Error Creating Session");
			}
			try {

				trns = session.beginTransaction();

				try {
					Notes test = null;
					test = (Notes) session.get(Notes.class, title);
					if (test == null)
						throw new NoteException("Note doesn't exist");
					session.delete(test);

				} catch (HibernateError e) {
					throw new NoteException(e.toString());
				}
				session.getTransaction().commit();
				return true;
			} catch (RuntimeException e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.flush();
				session.close();
			}

		} catch (ExceptionInInitializerError e) {
			e.printStackTrace();

		}

		return false;

	}

	@Override
	public ArrayList<String[]> getNotes(String login) throws NoteException {
		ArrayList<String[]> result = new ArrayList<>();

		try {
			Transaction trns = null;
			Session session = null;
			session = HibernateUtil.getSessionFactory().openSession();
			if (session == null) {
				throw new NoteException("Error Creating Session");
			}
			try {
				trns = session.beginTransaction();

				if (login == null) {
					throw new NoteException("Error with login!");
				}
				// Notes note = new Notes(login);
				try {

					Query query = session
							.createQuery("Select title from Notes where login = :login");
					query.setParameter("login", login);

					@SuppressWarnings("unchecked")
					List<String> list = query.list();

					for (String s : list) {
						Notes note = (Notes) session.get(Notes.class, s);
						note.setDate(note.getDate().substring(0,
								note.getDate().length() - 2));
						result.add(note.toStrings());

					}

				} catch (HibernateError e) {
					throw new NoteException(e.toString());
				}
				session.getTransaction().commit();
				return result;
				// result 0 - login, 1 - title, 2 - text, 3 - d
			} catch (RuntimeException e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.flush();
				session.close();
			}

		} catch (ExceptionInInitializerError e) {
			e.printStackTrace();

		}

		return result;
	}

	@Override
	public boolean deleteAccount(String login) throws NoteException {

		try {
			Transaction trns = null;
			Session session = null;
			session = HibernateUtil.getSessionFactory().openSession();
			if (session == null) {
				throw new NoteException("Error Creating Session");
			}
			try {

				trns = session.beginTransaction();

				if (login == null) {
					throw new NoteException("Error with login!");
				}

				Query query = session
						.createQuery("Select title from Notes where login = :login");
				query.setParameter("login", login);

				@SuppressWarnings("unchecked")
				List<String> list = query.list();

				for (String s : list) {
					Notes note = (Notes) session.get(Notes.class, s);
					session.delete(note);

				}
				session.getTransaction().commit();
				return true;

			} catch (RuntimeException e) {
				if (trns != null) {
					trns.rollback();
				}
				e.printStackTrace();
			} finally {
				session.flush();
				session.close();
			}

		} catch (ExceptionInInitializerError e) {
			e.printStackTrace();

		}

		return false;
	}
}
